import { Component, OnInit } from '@angular/core';
import { School } from '../models/school.model';
import { SchoolsService } from '../api/schools.service';
import { Observable } from 'rxjs';

import { SharedDataService } from '../services/shared-data.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

  searchTerm: string = '';

  schools$ = new Observable<School[]>();

  favoritesSchools: Map<number, School> = new Map();

  ngOnInit() {
    this.sharedDataService.sharedData$.subscribe((dados) => {
      this.favoritesSchools = dados;
    });
  }

  updateFavorite(cod: number, school: School): void {
    if(this.favoritesSchools.has(cod)) {
      this.favoritesSchools.delete(cod);
    } else {
      this.favoritesSchools.set(cod, school);
    }
    this.sharedDataService.updateSharedData(this.favoritesSchools);
  }

  search(): void {
    console.log('asdi s')
    const isInepCode: boolean = /^\d+$/.test(this.searchTerm);

    const coEntidade: string = isInepCode ? this.searchTerm : '';

    const noEntidade: string = isInepCode ? '' : this.searchTerm;

    this.schools$ = this.schoolsService.getSchools(coEntidade, noEntidade);
  }

  constructor(private schoolsService: SchoolsService, private sharedDataService: SharedDataService) {}


}
