import { Component } from '@angular/core';
import { SharedDataService } from '../services/shared-data.service';
import { School } from '../models/school.model';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  favoritesSchools: Map<number, School> = new Map();

  ngOnInit() {
    this.sharedDataService.sharedData$.subscribe((dados) => {
      this.favoritesSchools = dados;
    });
  }

  updateFavorite(cod: number, school: School): void {
    if(this.favoritesSchools.has(cod)) {
      this.favoritesSchools.delete(cod);
    } else {
      this.favoritesSchools.set(cod, school);
    }
    this.sharedDataService.updateSharedData(this.favoritesSchools);
  }

  constructor(private sharedDataService: SharedDataService) { }

}
