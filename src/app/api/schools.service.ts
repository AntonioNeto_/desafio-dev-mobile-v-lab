import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { School } from '../models/school.model';

@Injectable({
  providedIn: 'root'
})
export class SchoolsService {

  private url = environment.api

  constructor(private httpClient: HttpClient) { }

  getSchools(coEntidade: string, noEntidade: string ) {
    const params = { coEntidade, noEntidade };
    return this.httpClient.get<School[]>(this.url, { params })
  }

}
