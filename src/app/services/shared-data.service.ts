// shared-data.service.ts
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { School } from '../models/school.model';

@Injectable({
  providedIn: 'root',
})
export class SharedDataService {
  private _sharedData = new BehaviorSubject<Map<number, School>>(new Map<number, School>());

  get sharedData$() {
    return this._sharedData.asObservable();
  }

  updateSharedData(newData: Map<number, School>) {
    this._sharedData.next(newData);
  }
}
